package com.devcamp.task6070.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    CCustomer findOrderByCustomerId (long customerId);
}
